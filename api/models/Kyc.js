const mongoose = require('mongoose');
const kycSchema = mongoose.Schema({
     _id: mongoose.Schema.Types.ObjectId,
    email:{type: String, required: true},
    cacdocument:Array,
    photoId1:Array,
    photoId2:Array,
    Memart:Array,
    companyname:{type: String, required: false},
    companydescription:{type: String, required: false},
    companyaddress:{type: String, required: false},


    created: {
        type: Date,
        default: Date.now
    }
});


module.exports = mongoose.model('Kyc', kycSchema);