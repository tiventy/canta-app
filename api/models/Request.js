const mongoose = require('mongoose');
const requestSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    amount:{type: String, required: true},
    quantity:{type: String, required: true},
    rate:{type: String, required: true},
    expiry:{type: String, required: true},
    created: {
        type: Date,
        default: Date.now
    }
});


module.exports = mongoose.model('Request', requestSchema);