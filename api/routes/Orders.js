var express = require('express');
var router = express.Router();
const CheckAuth = require('../middleware/checkauth');
const usercontroller = require('../controller/Orders');



router.post('/postorder',CheckAuth,usercontroller.post_orders);

router.get('/getall',CheckAuth,usercontroller.find_all_orders );

router.get('/getorders',usercontroller.get_orders);



module.exports = router;