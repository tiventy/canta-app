const mongoose = require('mongoose');



const bidSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email:{type: String, required: true},
    amount:{type: String, required: true},
    quantity:{type: String, required: true, unique: true,
    },
    rate:{type: String, required: true},
    created: {
        type: Date,
        default: Date.now
    }
});




module.exports = mongoose.model('Bids', bidSchema);