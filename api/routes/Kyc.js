var express = require('express');
var router = express.Router();
var multer = require('multer');
const uuidv4 = require('uuidv4');
const CheckAuth = require('../middleware/checkauth');
const kyc = require('../controller/Kyc');
const fs = require('fs');
const admzip = require('adm-zip');


// const make = fs.mkdir('./uploads/', { recursive: true }, (err) => {
//     if (err) throw err;
// });
const storage = multer.diskStorage({
    destination: function (req,file,cb) {
        cb(null,'./uploads/');
    },
    filename: function (req, file, cb){
        cb(null, new Date().toISOString() + file.originalname);
    }



});
const fileFilter = (req, res, cb) =>
{

    // make();

    if (file.mimeType === 'image/jpeg' || file.mimeType === 'image/png'){
        cb(null, true);
    } else{
        cb(null, false);
    }


};
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 10,
        //fileFilter: fileFilter
    }

});



router.post('/',upload.fields([{
    name: 'Id1', maxCount: 1
}, {
    name: 'Id2', maxCount: 1
}, {name: 'Memart', maxCount:1}, {name: 'CacDoc', maxCount:1}]),CheckAuth,kyc.post_kyc);







module.exports = router;
