var express = require('express');
 var router = express.Router();
 const CheckAuth = require('../middleware/checkauth');
const acl = require('../middleware/acl');
 const usercontroller = require('../controller/user');





router.get('/getall',CheckAuth,acl.ensureRole("superadmin"),usercontroller.user_get_all );
router.post('/signup',usercontroller.user_post_by_user);
router.post('/signupbymarketer',CheckAuth,acl.ensureRole("marketers"),usercontroller.user_post_for_marketers);
router.post('/login',usercontroller.user_login);



router.put('/:id/update',CheckAuth,usercontroller.user_put);
router.get('/:userid',CheckAuth,usercontroller.user_get_id);
router.delete('/:userid',CheckAuth,usercontroller.user_delete);





module.exports = router;