const Users = require('../models/users');

const jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt-nodejs');


exports.user_get_all = (req, res, next) => {
   var id = req.Userdata;
   console.log(id);

    Users.find()
.select()
    .exec()
    .then(docs => {
      res.status(200).json(
        {
          ResponseCode: "200",
          count: docs.length,
          user: docs.map(doc => {

            return {

              user: doc,
              request: {
                type: 'GET',
                url: 'http://localhost:3000/user/' + doc._id
              }
            }
          }),


        })
    }).catch(err => {
    res.status(500).json({
      ResponseCode: "500",
      message: err
    });
  });

};

exports.user_login = (req, res, next) => {


    var id = req.Userdata;
    console.log(id);
    var email = req.body.email;
    //var phone = req.body.phone;

    Users.find({email}).exec().then(user => {
        if (user.length < 1) {
            return res.status(404).json({
                ResponseCode: "422",
                message: "incorrect username or password"
            });
        }




        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
            if (err) {
                return res.status(404).json({
                    ResponseCode: "422",
                    message: "incorrect username or password"

                });
            }
            // if (!user[0].active) {
            //      return res.json({ success: false, message: 'Account is not yet activated. Please check your e-mail for activation link.'}); // Account is not activated
            //  }

            if (result) {
                const token = jwt.sign({
                        email:user[0].email,
                        userId: user[0]._id,
                        roles: user[0].roles
                    }, process.env.JWT_TOKEN,
                    {
                        //expiresIn: "1h"
                    });

                return res.status(200).json({
                    ResponseCode: "200",
                    message: "successful",
                    token: token,
                    user: user
                });

            }

            return res.status(404).json({
                ResponseCode: "401",
                message: "incorrect username or password"
            });
        })
    }).catch(err => {
            console.log(err);
            res.status(500).json({
                ResponseCode: "500",
                error: err
            });

        }

    )
    ;
};

exports.user_post_by_user = (req, res) => {
  if (req.file)
    console.log(req.file);

  Users.find({email: req.body.email}).exec().then(user => {
    if (user.length >= 1) {
      return res.status(200).json({
        ResponseCode: "422",
        message: "email exist"
      });
    }
    Users.find({phone: req.body.phone}).exec().then(create => {
      if (create.length >= 1) {
        return res.status(200).json({
          ResponseCode: "422",
          message: "phone number already exist"
        });
      } else {
        const user = new Users(req.body);
        user
          .save()
          .then(result => {
            console.log(result);
            res.status(200).json({ResponseCode: "200", message: "user registration successful", createduser: user});
          })
          .catch(err => res.status(500).json({ResponseCode: "500", error: err, message: "registration not successful"}));
      }
    })
  });
};

exports.user_get_id = (req, res, next) => {
  var id = req.params.userid;

  Users.findById(id)
    .exec()
    .then(doc => {
      console.log(doc)
      res.status(200).json({
        ResponseCode: "200",
        doc: doc
      })
    }).catch(err => {
    console.log(err)
    res.status(500).json({ResponseCode: "500", error: err});
  });

}

exports.user_delete = (req, res, next) => {
  var id = req.params.userid;
  Users.remove({_id: id}).exec().then(doc => {
    res.status(200).json({
      ResponseCode: "200",
      message: "user deleted"
    })
  }).catch(err => {
    console.log(err)
    res.status(500).json({
      ResponseCode: "500",
      message: err
    })
  });

}

exports.user_put = (req, res, next) => {

  var id = req.params.id;

  Users.findByIdAndUpdate(id, {$set: req.body}).select('email name').then(user => {

    console.log(user)
    res.status(200).json({
      ResponseCode: "200",
      message: user
    })
  }).catch(error => res.status(500).json({
    ResponseCode: "500",
    message: error
  }));
}



exports.user_post_for_marketers = (req, res) => {
    if (req.file)
        console.log(req.file);

    Users.find({email: req.body.email}).exec().then(user => {
        if (user.length >= 1) {
            return res.status(200).json({
                ResponseCode: "422",
                message: "email exist"
            });
        }
        Users.find({phone: req.body.phone}).exec().then(create => {
            if (create.length >= 1) {
                return res.status(200).json({
                    ResponseCode: "422",
                    message: "phone number already exist"
                });
            } else {
                const user = new Users(req.body);
                user
                    .save()
                    .then(result => {
                        console.log(result);
                        res.status(200).json({ResponseCode: "200", message: "user registration successful", createduser: user});
                    })
                    .catch(err => res.status(500).json({ResponseCode: "500", error: err, message: "registration not successful"}));
            }
        })
    });
};
