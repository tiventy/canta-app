var express = require('express');
var router = express.Router();
var multer = require('multer');
const CheckAuth = require('../middleware/checkauth');
const usercontroller = require('../controller/Bids');



router.post('/',CheckAuth,usercontroller.post_bids);

router.get('/',CheckAuth,usercontroller.get_all_bid);

router.get('/getbids',CheckAuth,usercontroller.get_bids);





module.exports = router;