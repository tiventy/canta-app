const mongoose = require('mongoose');
const BeneficiarySchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email:{type: String, required: true},
    Accounttype:{type: String, required: true},
    AccountName:{type: String, required: true},
    Beneficiaryemail:{type: String, required: true},
    Currency:{type: String, required: true},
    Accountnumber:{type: String, required: true},
    Bankname:{type: String, required: true},
    BeneficiaryAddress:{type: String, required: true},
    Beneficiarycity:{type: String, required: true},
    Description:{type: String, required: true},
    created: {
        type: Date,
        default: Date.now
    }
});


module.exports = mongoose.model('Beneficiary', BeneficiarySchema);