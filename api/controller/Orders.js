const Orders = require('../models/Orders');


exports.post_orders = (req, res, next) =>
{
    var id = req.Userdata.userId;
    console.log(id);

    const bid = new Orders({
        _id: id,
        amount: req.body.amount,
        quantity: req.body.quantity,
        rate: req.body.rate,
        expiry: req.body.expiry

    });
    bid
        .save()
        .then(result => {
            console.log(result);
        })
        .catch(err =>
            console.log(err));

}



exports.find_all_orders = (req, res, next) => {
    Orders.find()
        .select('user _id email')
        .exec()
        .then(docs => {
            res.status(200).json(
                {
                    ResponseCode: "200",
                    count: docs.length,
                    user: docs.map(doc => {

                        return {

                            event: doc,
                            request: {
                                type: 'GET',
                                url: 'http://localhost:3000/bid/' + doc._id
                            }
                        }
                    }),


                })
        }).catch(err => {
        res.status(500).json({
            ResponseCode: "500",
            message: err
        });
    });

};


exports.get_orders = (req, res) => {

    var id = req.Userdata.email;

    // console.log(id);

    Orders.find({email: id}).select().exec().then(doc => {
        req.userRes = doc[0];

        res.status(200).json({
            ResponseCode: "200",
            doc: doc

        })
    }).catch(err => {
        res.status(500).json({
            ResponseCode: "500",
            message: err
        })
    });
}
