const Bids = require('../models/Bids');


exports.post_bids = (req, res, next) =>
{
    var id = req.Userdata.userId;

    console.log(id);

    const bid = new Bids({
        _id: id,
        amount: req.body.amount,
        quantity: req.body.quantity,
        rate: req.body.rate
    });
    bid
        .save()
        .then(result => {
            console.log(result);
        })
        .catch(err =>
            console.log(err));

}


exports.get_all_bid = (req, res, next) => {

    var id = req.Userdata.email;
    Bids.find({email: id})
        .select()
        .exec()
        .then(docs => {
            res.status(200).json(
                {
                    ResponseCode: "200",
                    count: docs.length,
                    user: docs.map(doc => {

                        return {

                            bid: doc,
                            request: {
                                type: 'GET',
                                url: 'http://localhost:3000/bid/' + doc._id
                            }
                        }
                    }),


                })
        }).catch(err => {
        res.status(500).json({
            ResponseCode: "500",
            message: err
        });
    });

};


exports.get_bids = (req, res) => {

    var id = req.Userdata.email;

    // console.log(id);

    Bids.find({email: id}).select().exec().then(doc => {
        req.userRes = doc[0];

        res.status(200).json({
            ResponseCode: "200",
            doc: doc

        })
    }).catch(err => {
        res.status(500).json({
            ResponseCode: "500",
            message: err
        })
    });
}
